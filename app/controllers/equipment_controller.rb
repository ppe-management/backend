require 'zip'

class EquipmentController < ApplicationController
  before_action :set_equipment, only: [:show, :update, :destroy]

  # GET /equipment
  def index
    @equipment = Equipment.all

    respond_to do |format|
      format.json do
        render json: @equipment
      end
      format.zip do
        zip = Zip::OutputStream::write_buffer do |zio|
          @equipment.each do |equipment|
            zio.put_next_entry("epi/equipemnts/#{equipment.category.name}/#{equipment.identifier}.txt")
            [[:identifier, 'Identifiant'], [:make, 'Fabricant'], [:model, 'Modèle'], [:serial, 'Numéro de série'], [:description, 'Description'], [:manufacturing_date, 'Date de fabrication'], [:purchase_date, 'Date d\'achat'], [:first_use_date, 'Date de première utilisation']].each do |attribute|
              zio.write "#{attribute[1]} : #{equipment.send attribute[0]}\n"
            end
            zio.write "\nLot\n===\n\n" unless equipment.batch_parts.empty?
            equipment.batch_parts.each do |batch_part|
              zio.write "\n"
              [[:identifier, 'Identifiant'], [:serial, 'Numéro de série'], [:description, 'Description']].each do |attribute|
                zio.write "#{attribute[1]} : #{batch_part.send attribute[0]}\n"
              end
            end
            zio.write "\nFichiers liés\n=============\n" unless equipment.attachments.empty?
            equipment.attachments.each do |attachment|
              zio.write "\n"
              zio.write "#{attachment.name}: #{attachment.file_identifier}\n"
              zio.write "Catégorie : #{attachment.category.name}\n"
            end
            zio.write "\nVérifications\n=============n\n" unless equipment.inspections.empty?
            equipment.inspections.each do |inspection|
              zio.write "\n"
              [[:date, 'Date'], [:event, 'Événement marquant'], [:description, 'Description']].each do |attribute|
                zio.write "#{attribute[1]} : #{inspection.send attribute[0]}\n"
              end
              if inspection.batch_parts.empty?
                zio.write "Résultat : "
                if inspection.pass
                  zio.write "OK\n"
                else
                  zio.write "Rebut\n"
                end
              else
                inspection.batch_parts.each do |batch_part|
                  zio.write "\n"
                  zio.write "Identifiant : #{batch_part.equipment_batch_part.identifier}\n"
                  [[:description, 'Description']].each do |attribute|
                    zio.write "#{attribute[1]} : #{batch_part.send attribute[0]}\n"
                  end
                  zio.write "Résultat : "
                  if batch_part.pass
                    zio.write "OK\n"
                  else
                    zio.write "Rebut\n"
                  end
                end
              end
            end
          end
          Attachment.all.each do |attachment|
            zio.put_next_entry "epi/fichiers/#{attachment.file_identifier}"
            zio.write IO.read attachment.file.current_path
          end
        end
        send_data zip.string
      end
    end
  end

  # GET /equipment/1
  def show
    render json: @equipment
  end

  # POST /equipment
  def create
    @equipment = Equipment.new(equipment_params)

    if @equipment.save
      render json: @equipment, status: :created, location: @equipment
    else
      render json: @equipment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /equipment/1
  def update
    if @equipment.update(equipment_params)
      render json: @equipment
    else
      render json: @equipment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /equipment/1
  def destroy
    @equipment.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_equipment
      @equipment = Equipment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def equipment_params
      params.require(:equipment).permit(:identifier, :make, :model, :serial, :description, :category_id, :manufacturing_date, :purchase_date, :first_use_date, :end_of_use_date)
    end
end

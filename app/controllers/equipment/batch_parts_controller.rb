class Equipment::BatchPartsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_equipment
  before_action :set_batch_part, only: [:show, :update, :destroy]

  # GET /equipment/1/batch_parts
  def index
    @batch_parts = @equipment.batch_parts

    render json: @batch_parts
  end

  # GET /equipment_batch_parts/1
  def show
    render json: @batch_part
  end

  # POST /equipment_batch_parts
  def create
    @batch_part = @equipment.batch_parts.build(batch_part_params)

    if @batch_part.save
      render json: @batch_part, status: :created, location: equipment_batch_part_url(@equipment, @batch_part)
    else
      render json: @batch_part.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /equipment/1/batch_parts/1
  def update
    if @batch_part.update(batch_part_params)
      render json: @batch_part
    else
      render json: @batch_part.errors, status: :unprocessable_entity
    end
  end

  # DELETE /equipment_batch_parts/1
  def destroy
    @batch_part.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_equipment
      @equipment = Equipment.find(params[:equipment_id])
    end

    def set_batch_part
      @batch_part = @equipment.batch_parts.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def batch_part_params
      params.require(:batch_part).permit(:identifier, :serial, :description, :equipment_id)
    end
end

class Equipment::AttachmentsController < ApplicationController
  before_action :set_equipment

  # GET /equipment/1/attachments
  def index
    @attachments = @equipment.attachments

    render json: @attachments
  end

  # POST /equipment/1/attachments/1
  def add
    attachment = Attachment.find(params[:id])
    @equipment.attachments.push attachment
    @equipment.save
  end

  # DELETE /equipment_attachments/1
  def remove
    attachment = @equipment.attachments.find(params[:id])
    @equipment.attachments.delete attachment
    @equipment.save
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_equipment
      @equipment = Equipment.find(params[:equipment_id])
    end
end

class Inspections::BatchPartsController < ApplicationController
  before_action :set_inspection
  before_action :set_inspection_batch_part, only: [:show, :update, :destroy]

  # GET /equipment/1//inspections/1/batch_parts
  def index
    batch_parts = @inspection.batch_parts

    render json: batch_parts
  end

  # GET /equipment/1/inspections/1/batch_parts/1
  def show
    render json: batch_part
  end

  # POST /equipment/1/inspections/1/batch_parts
  def create
    batch_part = @inspection.batch_parts.build(inspection_batch_part_params)

    if batch_part.save
      render json: batch_part, status: :created, location: equipment_inspection_batch_part_url(@equipment, @inspection, batch_part)
    else
      render json: batch_part.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /equipment/1/inspections/1/batch_parts/1
  def update
    if @batch_part.update(inspection_batch_part_params)
      render json: @batch_part
    else
      render json: @batch_part.errors, status: :unprocessable_entity
    end
  end

  # DELETE /equipment/1/inspections/1/batch_parts/1
  def destroy
    batch_part.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_inspection
      @equipment = Equipment.find(params[:equipment_id])
      @inspection = @equipment.inspections.find(params[:inspection_id])
    end

    def set_inspection_batch_part
      @batch_part = @inspection.batch_parts.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def inspection_batch_part_params
      params.require(:batch_part).permit(:description, :pass, :inspection_id, :equipment_batch_part_id)
    end
end

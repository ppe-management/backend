class InspectionsController < ApplicationController
  before_action :set_equipment
  before_action :set_inspection, only: [:show, :update, :destroy]

  # GET /equipment/1/inspections
  def index
    @inspections = @equipment.inspections

    render json: @inspections
  end

  # GET /equipment/1/inspections/1
  def show
    render json: @inspection
  end

  # POST /equipment/1/inspections
  def create
    @inspection = @equipment.inspections.build(inspection_params)

    if @inspection.save
      render json: @inspection, status: :created, location: [@equipment, @inspection]
    else
      render json: @inspection.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /equipment/1/inspections/1
  def update
    if @inspection.update(inspection_params)
      render json: @inspection
    else
      render json: @inspection.errors, status: :unprocessable_entity
    end
  end

  # DELETE /equipment/1/inspections/1
  def destroy
    @inspection.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_equipment
      @equipment = Equipment.find(params[:equipment_id])
    end

    def set_inspection
      @inspection = @equipment.inspections.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def inspection_params
      params.require(:inspection).permit(:date, :description, :pass, :event, :equipment_id)
    end
end

class Attachments::CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :update, :destroy]

  # GET /attachments/categories
  def index
    @categories = Attachment::Category.all

    render json: @categories
  end

  # GET /attachments/categories/1
  def show
    render json: @category
  end

  # POST /attachments/categories
  def create
    @category = Attachment::Category.new(category_params)

    if @category.save
      render json: @category, status: :created, location: @attachment_category
    else
      render json: @category.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /attachments/categories/1
  def update
    if @category.update(category_params)
      render json: @category
    else
      render json: @category.errors, status: :unprocessable_entity
    end
  end

  # DELETE /attachments/categories/1
  def destroy
    @category.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Attachment::Category.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def category_params
      params.require(:category).permit(:name)
    end
end

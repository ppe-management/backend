class Equipment::Category < ApplicationRecord
  has_many :equipment
  validates :name, presence: true, uniqueness: { case_sensitive: false }
end

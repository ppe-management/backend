class ModelUniquenessValidator < ActiveRecord::Validations::UniquenessValidator
  private

  def find_finder_class_for(record)
    options[:model]
  end
end

class Equipment::BatchPart < ApplicationRecord
  belongs_to :equipment
  has_many :inspections, dependent: :destroy, inverse_of: 'equipment_batch_part', class_name: 'Inspection::BatchPart', foreign_key: 'equipment_batch_part_id'

  validates :identifier, presence: true, uniqueness: { case_sensitive: false }, :model_uniqueness => { model: Equipment, case_sensitive: false }
end

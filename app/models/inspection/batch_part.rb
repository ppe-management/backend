class Inspection::BatchPart < ApplicationRecord
  belongs_to :inspection
  belongs_to :equipment_batch_part, inverse_of: :inspections, class_name: 'Equipment::BatchPart'
end

class Inspection < ApplicationRecord
  belongs_to :equipment
  has_many :batch_parts, dependent: :destroy, class_name: 'Inspection::BatchPart'

  validates :date, presence: true
end

class Attachment < ApplicationRecord
  belongs_to :category, class_name: 'Attachment::Category'
  has_and_belongs_to_many :equipment
  mount_uploader :file, AttachmentUploader
end

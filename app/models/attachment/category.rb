class Attachment::Category < ApplicationRecord
  has_many :attachment
  validates :name, presence: true, uniqueness: { case_sensitive: false }
end

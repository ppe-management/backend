class Equipment < ApplicationRecord
  belongs_to :category, class_name: 'Equipment::Category'
  has_many :batch_parts, dependent: :destroy, class_name: 'Equipment::BatchPart'
  has_many :inspections, dependent: :destroy
  has_and_belongs_to_many :attachments

  validates :identifier, presence: true, uniqueness: { case_sensitive: false }
  validates :make, presence: true
  validates :model, presence: true
  validates :manufacturing_date, presence: true
  validates :purchase_date, presence: true
  validates :first_use_date, presence: true
end

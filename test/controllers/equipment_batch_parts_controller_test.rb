require 'test_helper'

class EquipmentBatchPartsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @equipment_batch_part = equipment_batch_parts(:one)
  end

  test "should get index" do
    get equipment_batch_parts_url, as: :json
    assert_response :success
  end

  test "should create equipment_batch_part" do
    assert_difference('EquipmentBatchPart.count') do
      post equipment_batch_parts_url, params: { equipment_batch_part: { identifier: @equipment_batch_part.identifier, serial: @equipment_batch_part.serial } }, as: :json
    end

    assert_response 201
  end

  test "should show equipment_batch_part" do
    get equipment_batch_part_url(@equipment_batch_part), as: :json
    assert_response :success
  end

  test "should update equipment_batch_part" do
    patch equipment_batch_part_url(@equipment_batch_part), params: { equipment_batch_part: { identifier: @equipment_batch_part.identifier, serial: @equipment_batch_part.serial } }, as: :json
    assert_response 200
  end

  test "should destroy equipment_batch_part" do
    assert_difference('EquipmentBatchPart.count', -1) do
      delete equipment_batch_part_url(@equipment_batch_part), as: :json
    end

    assert_response 204
  end
end

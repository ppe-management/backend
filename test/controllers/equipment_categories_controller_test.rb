require 'test_helper'

class EquipmentCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @equipment_category = equipment_categories(:one)
  end

  test "should get index" do
    get equipment_categories_url, as: :json
    assert_response :success
  end

  test "should create equipment_category" do
    assert_difference('EquipmentCategory.count') do
      post equipment_categories_url, params: { equipment_category: { description: @equipment_category.description, name: @equipment_category.name } }, as: :json
    end

    assert_response 201
  end

  test "should show equipment_category" do
    get equipment_category_url(@equipment_category), as: :json
    assert_response :success
  end

  test "should update equipment_category" do
    patch equipment_category_url(@equipment_category), params: { equipment_category: { description: @equipment_category.description, name: @equipment_category.name } }, as: :json
    assert_response 200
  end

  test "should destroy equipment_category" do
    assert_difference('EquipmentCategory.count', -1) do
      delete equipment_category_url(@equipment_category), as: :json
    end

    assert_response 204
  end
end

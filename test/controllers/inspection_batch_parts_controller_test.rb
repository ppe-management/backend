require 'test_helper'

class InspectionBatchPartsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @inspection_batch_part = inspection_batch_parts(:one)
  end

  test "should get index" do
    get inspection_batch_parts_url, as: :json
    assert_response :success
  end

  test "should create inspection_batch_part" do
    assert_difference('InspectionBatchPart.count') do
      post inspection_batch_parts_url, params: { inspection_batch_part: { description: @inspection_batch_part.description, pass: @inspection_batch_part.pass } }, as: :json
    end

    assert_response 201
  end

  test "should show inspection_batch_part" do
    get inspection_batch_part_url(@inspection_batch_part), as: :json
    assert_response :success
  end

  test "should update inspection_batch_part" do
    patch inspection_batch_part_url(@inspection_batch_part), params: { inspection_batch_part: { description: @inspection_batch_part.description, pass: @inspection_batch_part.pass } }, as: :json
    assert_response 200
  end

  test "should destroy inspection_batch_part" do
    assert_difference('InspectionBatchPart.count', -1) do
      delete inspection_batch_part_url(@inspection_batch_part), as: :json
    end

    assert_response 204
  end
end

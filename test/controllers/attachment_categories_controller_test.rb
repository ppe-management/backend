require 'test_helper'

class AttachmentCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @attachment_category = attachment_categories(:one)
  end

  test "should get index" do
    get attachment_categories_url, as: :json
    assert_response :success
  end

  test "should create attachment_category" do
    assert_difference('AttachmentCategory.count') do
      post attachment_categories_url, params: { attachment_category: { description: @attachment_category.description, name: @attachment_category.name } }, as: :json
    end

    assert_response 201
  end

  test "should show attachment_category" do
    get attachment_category_url(@attachment_category), as: :json
    assert_response :success
  end

  test "should update attachment_category" do
    patch attachment_category_url(@attachment_category), params: { attachment_category: { description: @attachment_category.description, name: @attachment_category.name } }, as: :json
    assert_response 200
  end

  test "should destroy attachment_category" do
    assert_difference('AttachmentCategory.count', -1) do
      delete attachment_category_url(@attachment_category), as: :json
    end

    assert_response 204
  end
end

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_25_170619) do

  create_table "attachment_categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "attachments", force: :cascade do |t|
    t.string "name"
    t.integer "category_id"
    t.string "file"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_attachments_on_category_id"
  end

  create_table "attachments_equipment", id: false, force: :cascade do |t|
    t.integer "equipment_id"
    t.integer "attachment_id"
    t.index ["attachment_id"], name: "index_attachments_equipment_on_attachment_id"
    t.index ["equipment_id"], name: "index_attachments_equipment_on_equipment_id"
  end

  create_table "equipment", force: :cascade do |t|
    t.string "identifier"
    t.string "make"
    t.string "model"
    t.string "serial"
    t.text "description"
    t.integer "category_id"
    t.date "manufacturing_date"
    t.date "purchase_date"
    t.date "first_use_date"
    t.date "end_of_use_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "batch"
    t.index ["category_id"], name: "index_equipment_on_category_id"
  end

  create_table "equipment_batch_parts", force: :cascade do |t|
    t.string "identifier"
    t.string "serial"
    t.text "description"
    t.integer "equipment_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["equipment_id"], name: "index_equipment_batch_parts_on_equipment_id"
  end

  create_table "equipment_categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "inspection_batch_parts", force: :cascade do |t|
    t.text "description"
    t.boolean "pass"
    t.integer "inspection_id"
    t.integer "equipment_batch_part_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["equipment_batch_part_id"], name: "index_inspection_batch_parts_on_equipment_batch_part_id"
    t.index ["inspection_id"], name: "index_inspection_batch_parts_on_inspection_id"
  end

  create_table "inspections", force: :cascade do |t|
    t.date "date"
    t.text "description"
    t.boolean "pass"
    t.boolean "special"
    t.text "event"
    t.integer "equipment_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["equipment_id"], name: "index_inspections_on_equipment_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "authentication_token", limit: 30
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end

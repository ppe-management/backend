class CreateInspections < ActiveRecord::Migration[6.0]
  def change
    create_table :inspections do |t|
      t.date :date
      t.text :description
      t.boolean :pass
      t.text :event
      t.belongs_to :equipment

      t.timestamps
    end
  end
end

class CreateEquipment < ActiveRecord::Migration[6.0]
  def change
    create_table :equipment do |t|
      t.string :identifier, unique: true
      t.string :make
      t.string :model
      t.string :serial
      t.text :description
      t.belongs_to :category
      t.date :manufacturing_date
      t.date :purchase_date
      t.date :first_use_date
      t.date :end_of_use_date

      t.timestamps
    end
  end
end

class CreateEquipmentBatchParts < ActiveRecord::Migration[6.0]
  def change
    create_table :equipment_batch_parts do |t|
      t.string :identifier
      t.string :serial
      t.text :description
      t.belongs_to :equipment

      t.timestamps
    end
  end
end

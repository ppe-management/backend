class CreateAttachments < ActiveRecord::Migration[6.0]
  def change
    create_table :attachments do |t|
      t.string :name
      t.belongs_to :category
      t.string :file

      t.timestamps
    end

    create_table :attachments_equipment, id: false do |t|
      t.belongs_to :equipment
      t.belongs_to :attachment
    end
  end
end

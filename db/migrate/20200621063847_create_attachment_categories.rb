class CreateAttachmentCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :attachment_categories do |t|
      t.string :name

      t.timestamps
    end
  end
end

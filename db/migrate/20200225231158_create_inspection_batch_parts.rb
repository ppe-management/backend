class CreateInspectionBatchParts < ActiveRecord::Migration[6.0]
  def change
    create_table :inspection_batch_parts do |t|
      t.text :description
      t.boolean :pass
      t.belongs_to :inspection
      t.belongs_to :equipment_batch_part

      t.timestamps
    end
  end
end

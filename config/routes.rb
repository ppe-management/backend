Rails.application.routes.draw do
  devise_for :users, defaults: { format: :json }
  namespace 'equipment' do
    resources :categories
  end
  resources :equipment do
    scope module: 'equipment' do
      resources :batch_parts
      get 'attachments', to: 'attachments#index'
      post 'attachments/:id', to: 'attachments#add'
      delete 'attachments/:id', to: 'attachments#remove'
    end
    resources :inspections do
      scope module: 'inspections' do
        resources :batch_parts
      end
    end
  end
  namespace :attachments do
    resources :categories
  end
  resources :attachments
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
